'use strict';

describe('Controller: AppCtrlController', function() {

    //Load the ui.router module
    beforeEach(module('ui.router'));
    //Load the module
    beforeEach(module('core'));

    var AppCtrlController,
        scope;

    beforeEach(inject(function($controller, $rootScope) {
        scope = $rootScope.$new();
        AppCtrlController = $controller('AppCtrlController', {
        $scope: scope
        });
    }));

    it('should ...', function() {

    });
});
