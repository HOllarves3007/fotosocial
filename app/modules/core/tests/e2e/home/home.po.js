var homePage = (function(){


    /**
     * Navigate to the login page
     */
    homePage.prototype.goToHome = function(){
        browser.get('http://127.0.0.1:9000/#!/');
    };
    
    return homePage;
})();

module.exports = homePage;
